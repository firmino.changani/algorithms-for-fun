/**
 *
 * Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * You can return the answer in any order.
 */

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (nums, target) {
  const result = [];
  let i = 0;

  while (result.length === 0 && i < nums.length - 1) {
    i++;
    let j = 0;

    while (result.length === 0 && j < nums.length - 1) {
      if (i !== j && nums[i] + nums[j] === target) {
        result.push(i, j);
      }
      j++;
    }
  }

  return result.sort();
};

console.log("RESULT ---> ", twoSum([3, 2, 4], 6));
