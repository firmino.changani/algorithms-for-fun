const input = [73, 67, 38, 33];

function gradingStudents(grades = []) {
  for (let i = 0; i < grades.length; i++) {
    const nextMultiple = getNextMultipleOf(grades[i]);
    const difference = nextMultiple - grades[i];

    if (grades[i] >= 38 && difference < 3) {
      grades[i] = nextMultiple;
    }
  }

  return grades;
}

function getNextMultipleOf(value, base = 5) {
  let multiple = value;

  while (multiple % base !== 0) {
    multiple++;
  }

  return multiple;
}

console.log(gradingStudents(input));
